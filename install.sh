#! /bin/bash

NAME=manjaro-arm-flasher
if [ -z "$1" ]; then
    echo "Installing Manjaro ARM Flasher files..."

    sudo install -m 755 $NAME /usr/local/bin/
    sudo install -m 644 $NAME.desktop /usr/share/applications/$NAME.desktop
    sudo install -m 644 $NAME.png /usr/share/icons/hicolor/128x128/apps/

    echo "Manjaro ARM Flasher files installed successfully!"
fi

if [[ "$1" = "uninstall" ]]; then
    echo "Removing Manjaro ARM Flasher files..."
    sudo rm /usr/local/bin/$NAME
    sudo rm /usr/share/applications/$NAME.desktop
    sudo rm /usr/share/icons/hicolor/128x128/apps/$NAME.png

    echo "Manjaro ARM Flasher files removed successfully"
fi
